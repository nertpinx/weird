use std::env;
use std::path::Path;
use std::process::Command;

fn main() {
    let out_dir = env::var("OUT_DIR").unwrap();

    if !Command::new("gcc")
        .args(&["src/weird.c", "-c", "-fPIC", "-ggdb", "-o"])
        .arg(&format!("{}/weird.o", out_dir))
        .status()
        .unwrap()
        .success()
    {
        std::process::exit(1);
    }

    if !Command::new("ar")
        .args(&["crus", "libweird.a", "weird.o"])
        .current_dir(&Path::new(&out_dir))
        .status()
        .unwrap()
        .success()
    {
        std::process::exit(1);
    }

    println!("cargo:rustc-link-search=native={}", out_dir);
    println!("cargo:rustc-link-lib=static=weird");

    eprintln!("{}", out_dir);
}
