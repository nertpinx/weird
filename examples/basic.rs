use weird::*;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut weird = Weird::create()?;
    weird.set_debug_callback(Some(Box::new(|a| eprintln!("ASDF: {}", a))));
    //    println!("{}", weird.get_version());
    //    weird.set_debug_callback(None)?;
    Ok(())
}
