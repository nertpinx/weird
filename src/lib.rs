#[link(name = "weird")]
extern "C" {
    fn weird_create() -> *mut weird_handle;
    fn weird_set_debug_callback(
        handle: *mut weird_handle,
        data: *mut c_void,
        debug_fn: Option<extern "C" fn(data: *mut c_void, msg: *const c_char) -> c_int>,
    );
    fn weird_close(handle: *mut weird_handle);
}

#[repr(C)]
#[derive(Debug)]
struct weird_handle {
    _unused: [u8; 0],
}

pub struct Weird {
    handle: *mut weird_handle,
    debug_callback: Option<Box<dyn Fn(&'static str)>>,
}

use std::ffi::CStr;
use std::os::raw::{c_char, c_int, c_void};

impl Weird {
    pub fn create() -> Result<Self, &'static str> {
        let handle = unsafe { weird_create() };
        if handle.is_null() {
            Err("Error creating handle")
        } else {
            Ok(Self {
                handle: handle,
                debug_callback: None,
            })
        }
    }
}

impl Drop for Weird {
    fn drop(&mut self) {
        unsafe {
            weird_close(self.handle);
        }
    }
}

impl Weird {
    /// Using a dynamic dispatch with a trait object so that it can be called
    /// like this:
    /// ```
    /// let mut weird = weird::Weird::create().unwrap();
    /// weird.set_debug_callback(None);
    /// ```
    pub fn set_debug_callback(&mut self, debug_fn: Option<Box<dyn Fn(&'static str)>>) {
        extern "C" fn cb_wrapper(data: *mut c_void, msg: *const c_char) -> c_int {
            let callback: &mut Box<dyn Fn(&'static str)> = unsafe { std::mem::transmute(data) };
            let msg = unsafe { CStr::from_ptr(msg) }.to_str().unwrap();
            callback(msg);
            0
        };

        match debug_fn {
            Some(f) => {
                let callback = Box::into_raw(Box::new(f));
                self.debug_callback = Some(unsafe { Box::from_raw(callback) });
                unsafe {
                    weird_set_debug_callback(self.handle, callback as *mut _, Some(cb_wrapper));
                }
            }
            None => {
                self.debug_callback = None;
                unsafe {
                    weird_set_debug_callback(self.handle, std::ptr::null_mut(), None);
                }
            }
        }
    }
}
