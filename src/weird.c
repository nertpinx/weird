#include <stdlib.h>

typedef void (*debug_cb)(void *opaque, const char *message);

struct weird_handle {
    void *opaque;
    debug_cb callback;
};

struct weird_handle *
weird_create()
{
    return (struct weird_handle *)calloc(1, sizeof(struct weird_handle));
}

void
weird_set_debug_callback(struct weird_handle *handle,
                         void *opaque,
                         debug_cb* callback)
{
    handle->callback = callback;
    handle->opaque = opaque;
}

void
weird_close(struct weird_handle *handle)
{
    if (handle->callback) {
        handle->callback(handle->opaque, "weird: debug: closing");
    }
    free(handle);
}
